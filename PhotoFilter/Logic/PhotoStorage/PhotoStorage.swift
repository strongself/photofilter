//
//  PhotoStorage.swift
//  PhotoFIlter
//
//  Created by Nikolay Gladkovskiy on 11/11/2018.
//  Copyright © 2018 Nikolay Gladkovskiy. All rights reserved.
//

import UIKit

class PhotoStorage
{
    private var draftPhotos = [String: Photo]()
    
    private let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0].appending("/latestDraftPhoto.jpg")
    
    init()
    {
        guard let latestDraftImage = UIImage(contentsOfFile: documentPath) else {return}
        
        let photo = Photo.init(from: latestDraftImage)
        
        draftPhotos["testPhoto"] = photo
    }
    
    open func getPhoto(with identifier: String) -> Photo?
    {
        return draftPhotos[identifier]
    }
    
    open func removePhoto(with identifier: String?)
    {
        do
        {
            let fileManager = FileManager.default
            try fileManager.removeItem(atPath: documentPath)
        }
        catch
        {
            print(error)
        }
        draftPhotos = [String: Photo]()
    }
    
    open func saveDraft(_ photo: UIImage)
    {
        do {
            let fileURL = URL.init(fileURLWithPath: documentPath)
            let imageData = photo.jpegData(compressionQuality: 0.85)
            try imageData?.write(to: fileURL)
        }
        catch
        {
            print(error)
        }
        
        let savedPhoto = Photo.init(from: photo)
        draftPhotos["testPhoto"] = savedPhoto
    }
}

struct Photo
{
    let fullsizePhoto: UIImage
    let smallsizePhoto: UIImage
    
    init(from image: UIImage)
    {
        let orientedPhoto = image.fixedOrientation()
        self.fullsizePhoto = orientedPhoto.cropToSquare()
        self.smallsizePhoto = fullsizePhoto.resizeImage(to: CGSize.init(width: 128, height: 128))
    }
}


extension UIImage
{
    func resizeImage(to targetSize:CGSize) -> UIImage
    {
        let originalSize = self.size
        
        let widthRatio = targetSize.width / originalSize.width
        let heightRatio = targetSize.height / originalSize.height
        
        let ratio = min(widthRatio, heightRatio)
        
        let newSize = CGSize(width: originalSize.width * ratio, height: originalSize.height * ratio)
        
        // preparing rect for new image size
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        self.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() ?? self
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func cropToSquare() -> UIImage
    {
        let originalWidth  = self.size.width
        let originalHeight = self.size.height
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        var edge: CGFloat = 0.0
        
        if (originalWidth > originalHeight) {
            // landscape
            edge = originalHeight
            x = (originalWidth - edge) / 2.0
            y = 0.0
            
        } else if (originalHeight > originalWidth) {
            // portrait
            edge = originalWidth
            x = 0.0
            y = (originalHeight - originalWidth) / 2.0
        } else {
            // square
            edge = originalWidth
        }
        
        let cropSquare = CGRect.init(x: x, y: y, width: edge, height: edge)
        guard  let cgImage = self.cgImage
            else
        {
            return self
        }
        
        let imageRef = cgImage.cropping(to: cropSquare)
        guard  let cropCgImage = imageRef
            else
        {
            return self
        }
        
        return UIImage(cgImage: cropCgImage, scale: UIScreen.main.scale, orientation: self.imageOrientation)
    }
        
        func fixedOrientation() -> UIImage {
            
            if imageOrientation == .up
            {
                return self
            }
            
            var transform: CGAffineTransform = CGAffineTransform.identity
            
            switch imageOrientation
            {
            case .down, .downMirrored:
                transform = transform.translatedBy(x: size.width, y: size.height)
                transform = transform.rotated(by: CGFloat.pi)
                break
            case .left, .leftMirrored:
                transform = transform.translatedBy(x: size.width, y: 0)
                transform = transform.rotated(by: CGFloat.pi/2)
                break
            case .right, .rightMirrored:
                transform = transform.translatedBy(x: 0, y: size.height)
                transform = transform.rotated(by: -CGFloat.pi/2)
                break
            case .up, .upMirrored:
                break
            }
            
            switch imageOrientation
            {
            case .upMirrored, .downMirrored:
                transform.translatedBy(x: size.width, y: 0)
                transform.scaledBy(x: -1, y: 1)
                break
            case .leftMirrored, .rightMirrored:
                transform.translatedBy(x: size.height, y: 0)
                transform.scaledBy(x: -1, y: 1)
            case .up, .down, .left, .right:
                break
            }
            
            let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: (self.cgImage?.colorSpace)!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
            
            ctx.concatenate(transform)
            
            switch imageOrientation
            {
            case .left, .leftMirrored, .right, .rightMirrored:
                ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
                break
            default:
                ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                break
            }
            
            let cgImage: CGImage = ctx.makeImage()!
            
            return UIImage(cgImage: cgImage)
        }
}
