//
//  Locator.swift
//  PhotoFIlter
//
//  Created by Nikolay Gladkovskiy on 11/11/2018.
//  Copyright © 2018 Nikolay Gladkovskiy. All rights reserved.
//

import Foundation

final class Locator
{
    static let sharedInstance = Locator()

    //MARK: Pubbllic
    
    let photoStorage: PhotoStorage!
    let filterStorage: FilterStorage!
    
    init() {
        photoStorage = PhotoStorage.init()
        filterStorage = FilterStorage()
    }
}
