//
//  FilterStorage.swift
//  PhotoFIlter
//
//  Created by Nikolay Gladkovskiy on 11/11/2018.
//  Copyright © 2018 Nikolay Gladkovskiy. All rights reserved.
//

import Foundation
import UIKit

class FilterStorage
{
    open func getFilters()-> [Filter]
    {
        return [
            Filter("", "Original"),
            Filter("CIPhotoEffectChrome", "Chrome"),
            Filter("CISepiaTone", "Sepia"),
            Filter("CIPhotoEffectTransfer", "Transfer"),
            Filter("CIPhotoEffectTonal", "Tonal"),
            Filter("CIPhotoEffectProcess", "Process"),
            Filter("CIPhotoEffectNoir", "Noir"),
            Filter("CIPhotoEffectInstant", "Instant"),
            Filter("CIPhotoEffectFade", "Fade")
        ]
    }
}

struct Filter
{
    let label: String
    let identifier: String
    let ciKey: String
    
    init(_ ciKey: String, _ label:String)
    {
        self.label = label
        self.identifier = UUID.init().uuidString
        self.ciKey = ciKey
    }
}

extension UIImage
{
    func applyFilter(_ filter: Filter?) -> UIImage
    {
        guard let newFilter = filter
            else
        {
            return self
        }
        
        let ciContext = CIContext(options: nil)
        let coreImage = CIImage(image: self)
        
        guard let filter = CIFilter(name: newFilter.ciKey)
            else
        {
            return self
        }
        
        filter.setDefaults()
        filter.setValue(coreImage, forKey: kCIInputImageKey)
        let filteredImageData = filter.value(forKey: kCIOutputImageKey) as! CIImage
        guard let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
            else
        {
            return self
        }
        return UIImage(cgImage: filteredImageRef);
    }
}
