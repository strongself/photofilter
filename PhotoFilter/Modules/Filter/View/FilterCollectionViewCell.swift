//
//  CommonSettingsTableCellView.swift
//  HomeStretch
//
//  Created by Nikolay Gladkovskiy on 28.08.2018.
//  Copyright © 2018 Homestretch AG. All rights reserved.
//

import UIKit


final class FilterCollectionViewCell: UICollectionViewCell
{
    // MARK: Outlets
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
}
