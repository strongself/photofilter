//
//  CommonSettingsViewInterface.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

import UIKit

protocol FilterViewInput: class
{
    func update(photo: UIImage?)
    
    func updateCells(_ data: [(String,UIImage)])
    
    func selectCell(indexPath: IndexPath)
    
    func showError(with text: String)
}


protocol FilterViewOutput: class
{    
    func viewDidReadyForEvents()
    
    func viewDidPressOnCell(at: IndexPath)

    func viewDidPressOnShareButton()
}
