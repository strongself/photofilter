//
//  CommonSettingsViewController.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

import UIKit


class FilterViewController: UIViewController
{
    var moduleInput: FilterModuleInput!
    var output: FilterViewOutput!
    var cellsData = [(String, UIImage)]()
    //MARK: Outlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var distructiveButton: UIButton!
    
    @IBAction func distructiveButtonAction(_ sender: UIButton)
    {
        output.viewDidPressOnShareButton()
    }
    
    override internal var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        output.viewDidReadyForEvents()
    }
    
    override func viewDidLoad() {
        self.collectionView.register(UINib(nibName: "FilterCollectionViewCell", bundle: nil),
                                        forCellWithReuseIdentifier: "FilterCell")
    }
}

private extension FilterViewController
{
    
}


extension FilterViewController: UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        output.viewDidPressOnCell(at: indexPath)
    }
}


extension FilterViewController: UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return cellsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath)
        return collectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        guard let collectionViewCell = cell as? FilterCollectionViewCell else {return}
        
        let cellData = cellsData[indexPath.item]
        
        collectionViewCell.label.text = cellData.0
        collectionViewCell.ImageView.image = cellData.1
    }
}


extension FilterViewController: FilterViewInput
{
    func update(photo: UIImage?)
    {
        imageView.image = photo
    }
    
    func updateCells(_ data: [(String, UIImage)])
    {
        cellsData = data
        collectionView.reloadData()
    }
    
    func selectCell(indexPath: IndexPath)
    {
        if (indexPath.item < collectionView.numberOfItems(inSection: 0))
        {
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    func showError(with text: String)
    {
            let ac = UIAlertController(title: "Attention!",
                                       message: text,
                                       preferredStyle: .alert)
        
            ac.addAction(UIAlertAction(title: "OK",
                                       style: .default))
        
            present(ac, animated: true)
    }
}

private let kFilterCell = "FilterCell"
