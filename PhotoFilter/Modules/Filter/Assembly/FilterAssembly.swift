//
//  CommonSettingsAssembly.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

import UIKit


class FilterAssembly: NSObject
{
    func viewFilterModule() -> FilterViewController
    {
        let locator = Locator.sharedInstance
        
        let vc = FilterViewController(nibName: "FilterVC", bundle: nil)
        let presenter = FilterPresenter()
        
        let router = FilterRouter.init()
        let interactor = FilterInteractor.init(with: locator.photoStorage, filterStorage: locator.filterStorage)
        
        vc.moduleInput = presenter
        vc.output = presenter
        presenter.userInterface = vc
        
        interactor.output = presenter
        presenter.interactor = interactor
        
        router.viewController = vc
        presenter.router = router
        
        return vc;
    }
}
