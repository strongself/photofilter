//
//  CommonSettingsInteractor.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

import UIKit
import Foundation

class FilterInteractor: NSObject
{
    weak var output: FilterInteractorOutput!
    let photoStorage: PhotoStorage!
    let filterStorage: FilterStorage!
    
    private var availableFilters: Array<Filter>?
    private var selectedFilter: Filter?
    
    private var originalPhotoId: String?
    
    private var originalPhoto: UIImage?
    private var originalPhotoPreview: UIImage?
    
    private var filteredPhoto: UIImage?
    private var filteredPhotoPreviews = [(String,String,UIImage)]()
    
    required init(with photoStorage:PhotoStorage,
                       filterStorage:FilterStorage)
    {
        self.photoStorage = photoStorage
        self.filterStorage = filterStorage
        
        availableFilters = filterStorage.getFilters()
        
        selectedFilter = availableFilters?.first
        super.init()
    }
    
    private let filterPlaceHolder = UIImage.init(named: "filterPlaceholder")!
}

private extension FilterInteractor
{
    func makeFileredPhoto()
    {
        filteredPhoto = originalPhoto?.applyFilter(selectedFilter)
    }
    
    func makeFilteredPreviews()
    {

        
        guard let filters = availableFilters
            else
        {
            filteredPhotoPreviews.append(("none","Original",originalPhotoPreview ?? filterPlaceHolder))
            return
        }
        
        for filter in filters
        {
            filteredPhotoPreviews.append((filter.identifier,filter.label,originalPhotoPreview?.applyFilter(filter) ?? filterPlaceHolder))
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer)
    {
        if let error = error {
            output.interactor(caught: error)
        } else {
            output.interactorDidSave(photo: image)
            
            photoStorage.removePhoto(with: originalPhotoId)
        }
    }
}

extension FilterInteractor: FilterInteractorInput
{
    func configure(with photoId: String)
    {
        originalPhotoId = photoId
        
        let photo = photoStorage.getPhoto(with: photoId)
        
        originalPhoto = photo?.fullsizePhoto
        originalPhotoPreview = photo?.smallsizePhoto
        
        applyFilter(with: selectedFilter?.identifier)
        
        makeFilteredPreviews()
        output.interactorDidReload(filters: filteredPhotoPreviews)
    }
    
    func applyFilter(with identifier: String?)
    {
        selectedFilter = availableFilters?.first{$0.identifier == identifier}
        makeFileredPhoto()
        output.interactorDidLoad(photo: filteredPhoto, filterId: identifier)
    }
    
    func saveToLibrary()
    {
        guard let savedPhoto = filteredPhoto else {return}
        
        UIImageWriteToSavedPhotosAlbum(savedPhoto, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
}
