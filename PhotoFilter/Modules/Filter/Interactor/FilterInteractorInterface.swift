//
//  CommonSettingsInteractorInterface.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

import UIKit

protocol FilterInteractorInput: class
{
    func configure(with photoId: String)
    
    func applyFilter(with identifier: String?)
    
    func saveToLibrary()
}


protocol FilterInteractorOutput: class
{
    func interactorDidLoad(photo: UIImage?, filterId:String?)
    
    func interactorDidReload(filters:[(String,String,UIImage)]?)
    
    func interactor(caught error: Error)
    
    func interactorDidSave(photo: UIImage?)
}
