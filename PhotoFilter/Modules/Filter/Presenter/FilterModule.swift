//
//  FilterModule.swift
//  PhotoFIlter
//
//  Created by Nikolay Gladkovskiy on 11/11/2018.
//  Copyright © 2018 Nikolay Gladkovskiy. All rights reserved.
//

import UIKit

protocol FilterModuleInput: class
{
    func configure(with photoId: String)
}


protocol FilterModuleOutput: class
{
    func didSave(photoWithId: String)
}

