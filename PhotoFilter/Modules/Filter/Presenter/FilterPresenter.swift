//
//  CommonSettingsPresenter.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

import UIKit

class FilterPresenter
{
    weak var userInterface: FilterViewInput?
    var interactor: FilterInteractorInput!
    var router: FilterRouterInput!
    
    private var photo: UIImage?
    private var filterIndex: Int = 0
    
    private var filtersLabelsAndImages = [(String, UIImage)]()
    private var filtersIdentifiers = [String]()
}


extension FilterPresenter: FilterModuleInput
{
    func configure(with photoId: String) {
        interactor.configure(with: photoId)
    }
}


extension FilterPresenter: FilterViewOutput
{
    func viewDidReadyForEvents()
    {
        userInterface?.update(photo: photo)
        userInterface?.updateCells(filtersLabelsAndImages)
        userInterface?.selectCell(indexPath: IndexPath.init(item: filterIndex, section: 0))
    }
    
    func viewDidPressOnCell(at: IndexPath)
    {
        let selectedFilter = at.item == 0 ? nil : filtersIdentifiers[at.item]
        interactor.applyFilter(with: selectedFilter)
    }
    
    func viewDidPressOnShareButton()
    {
        interactor.saveToLibrary()
    }
}


extension FilterPresenter : FilterInteractorOutput
{
    func interactorDidLoad(photo: UIImage?, filterId: String?)
    {
        guard let newPhoto = photo else
        {
            return
        }
        self.photo = newPhoto
        userInterface?.update(photo: newPhoto)
        
        guard let newFilterId = filterId else
        {
            return
        }
        filterIndex = filtersIdentifiers.firstIndex(of: newFilterId) ?? 0
        userInterface?.selectCell(indexPath: IndexPath.init(item: filterIndex, section: 0))
    }
    
    func interactorDidReload(filters: [(String,String,UIImage)]?)
    {
        guard let photoFilters = filters else
        {
            userInterface?.updateCells(filtersLabelsAndImages)
            return
        }
        
        for filter in photoFilters
        {
            filtersLabelsAndImages.append((filter.1,filter.2))
            filtersIdentifiers.append(filter.0)
        }
    }
    
    func interactor(caught error: Error)
    {
        userInterface?.showError(with: error.localizedDescription)
    }
    
    func interactorDidSave(photo: UIImage?)
    {
        router.exit()
    }
}
