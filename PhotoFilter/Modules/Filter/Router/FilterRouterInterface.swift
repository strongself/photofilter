//
//  CommonSettingsRouterInterface.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

protocol FilterRouterInput: class
{
    func exit()
}
