//
//  CommonSettingsRouter.swift
//  Created by Nikolay Gladkovskiy on 28/08/2018.
//

import UIKit


class FilterRouter : NSObject
{
    weak var viewController: UIViewController!
    
}

extension FilterRouter: FilterRouterInput
{
    func exit()
    {
        self.viewController .dismiss(animated: true, completion: nil)
    }
}
