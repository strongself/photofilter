//
//  Photo.swift
//  PhotoFilter
//
//  Created by Nikolay Gladkovskiy on 12/11/2018.
//  Copyright © 2018 Nikolay Gladkovskiy. All rights reserved.
//

import UIKit

class TakeAPhotoModule: UIViewController, UINavigationControllerDelegate  {
    
    private var imagePicker: UIImagePickerController!
    private let locator = Locator.sharedInstance
    
    private enum ImageSource
    {
        case photoLibrary
        case camera
    }

    @IBAction func takePhoto(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1:
            selectImageFrom(.photoLibrary)
        default:
            selectImageFrom(.camera)
        }
    }
    
    private func selectImageFrom(_ source: ImageSource)
    {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source
        {
        case .camera:
            imagePicker.sourceType = .camera
            
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func presentFilterModule()
    {
        let filterModule = FilterAssembly().viewFilterModule()
        _ = filterModule.view
        filterModule.moduleInput.configure(with: "testPhoto")
        
        present(filterModule, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        if (locator.photoStorage.getPhoto(with: "testPhoto") != nil)
        {
            presentFilterModule()
        }
    }
}

extension TakeAPhotoModule: UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        imagePicker.dismiss(animated: true,
                            completion: nil)
        
        guard let selectedImage = info[.originalImage] as? UIImage else {return}
        
        locator.photoStorage.saveDraft(selectedImage)
        
        presentFilterModule()
    }
}
